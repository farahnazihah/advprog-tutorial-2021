# Lazy Instantiation vs Eager Instantiation

## Lazy Instantiation

Pada teknik ini, instance baru dibuat ketika dibutuhkan, bukan ketika class di-load. Alhasil tidak ada pembuatan object yang tidak akan terpakai. Sehingga teknik ini "menghemat" penggunaan memory. Namun akan mengurangi performance aplikasi

## Eager Instantiation

Pada teknik ini, insance dibuat ketika aplikasi baru dijalankan dan ketika class di-load. Teknik ini akan berguna jika object tersebut memang diperlukan pada setiap kasus. Aplikasi yang dijalankan sudah menyiapkan objects-nya dan siap menerima request, sehingga akan menghemat waktu eksekusi. Namun jika ternyata banyak object yang tidak digunakan, akan memakan banyak memory yang sebenarnya tidak diperlukan.
