package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class NoodleTest {

    private static List<Noodle> allNoodles;

    @BeforeAll
    public static void setUp(){
        allNoodles = new ArrayList<>();
        allNoodles.add(new Ramen());
        allNoodles.add(new Shirataki());
        allNoodles.add(new Soba());
        allNoodles.add(new Udon());
    }

    @Test
    public void testAllNoodlesAreConcreteClass() {
        for (Noodle concreteNoodle : allNoodles) {
            Class<?> concreteNoodleClass = concreteNoodle.getClass();

            assertFalse(Modifier.isAbstract(concreteNoodleClass.getModifiers()));
        }
    }

    @Test
    public void testAllNoodlesImplementsNoodle() {
        for (Noodle concreteNoodle : allNoodles) {
            Class<?> concreteNoodleClass = concreteNoodle.getClass();

            assertTrue(Noodle.class.isAssignableFrom(concreteNoodleClass));
        }
    }

    @Test
    public void testAllNoodlesHaveDifferentDescription() {
        HashSet<String> allDescriptions = new HashSet<>();

        for (Noodle concreteNoodle : allNoodles) {
            String description = concreteNoodle.getDescription();

            assertFalse(allDescriptions.contains(description));
            allDescriptions.add(description);
        }
    }

}
