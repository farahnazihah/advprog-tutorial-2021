package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MeatTest {

    private static List<Meat> allMeats;

    @BeforeAll
    public static void setUp(){
        allMeats = new ArrayList<>();
        allMeats.add(new Beef());
        allMeats.add(new Chicken());
        allMeats.add(new Fish());
        allMeats.add(new Pork());
    }

    @Test
    public void testAllMeatsAreConcreteClass() {
        for (Meat concreteMeat : allMeats) {
            Class<?> concreteMeatClass = concreteMeat.getClass();

            assertFalse(Modifier.isAbstract(concreteMeatClass.getModifiers()));
        }
    }

    @Test
    public void testAllMeatsImplementsMeat() {
        for (Meat concreteMeat : allMeats) {
            Class<?> concreteMeatClass = concreteMeat.getClass();

            assertTrue(Meat.class.isAssignableFrom(concreteMeatClass));
        }
    }

    @Test
    public void testAllMeatsHaveDifferentDescription() {
        HashSet<String> allDescriptions = new HashSet<>();

        for (Meat concreteMeat : allMeats) {
            String description = concreteMeat.getDescription();

            assertFalse(allDescriptions.contains(description));
            allDescriptions.add(description);
        }
    }

}
