package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class IngridientsFactoryTest {
    private Class<?> IngridientsFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        IngridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory");
    }

    @Test
    public void testIngridientsFactoryIsAPublicInterface() {
        int classModifiers = IngridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngridientsFactoryHasGetNoodleAbstractMethod() throws Exception {
        Method getNoodle = IngridientsFactoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testIngridientsFactoryHasGetMeatAbstractMethod() throws Exception {
        Method getMeat = IngridientsFactoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testIngridientsFactoryHasGetToppingAbstractMethod() throws Exception {
        Method getTopping = IngridientsFactoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testIngridientsFactoryHasGetFlavorAbstractMethod() throws Exception {
        Method getFlavor = IngridientsFactoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }
}
