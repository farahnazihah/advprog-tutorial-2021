package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class MenuTest {
    private Class<?> MenuClass;

    @BeforeEach
    public void setup() throws Exception {
        MenuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsAPublicConcrateClass() {
        int classModifiers = MenuClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testMenuHasGetNameConcrateMethod() throws Exception {
        Method getName = MenuClass.getDeclaredMethod("getName");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testMenuHasGetNoodleConcrateMethod() throws Exception {
        Method getNoodle = MenuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testMenuHasGetMeatConcrateMethod() throws Exception {
        Method getMeat = MenuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testMenuHasGetToppingConcrateMethod() throws Exception {
        Method getTopping = MenuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testMenuHasGetFlavorConcrateMethod() throws Exception {
        Method getFlavor = MenuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }

    @Test
    public void testMenuHasCreateMenuConcrateMethod() throws Exception {
        Method createMenu = MenuClass.getDeclaredMethod("createMenu");
        int methodModifiers = createMenu.getModifiers();

        assertTrue(Modifier.isPrivate(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createMenu.getParameterCount());
    }
}
