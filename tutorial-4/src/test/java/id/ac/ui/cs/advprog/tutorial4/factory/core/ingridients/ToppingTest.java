package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ToppingTest {

    private static List<Topping> allToppings;

    @BeforeAll
    public static void setUp() throws Exception{
        allToppings = new ArrayList<>();
        allToppings.add(new BoiledEgg());
        allToppings.add(new Cheese());
        allToppings.add(new Mushroom());
        allToppings.add(new Flower());
    }

    @Test
    public void testAllToppingsAreConcreteClass() {
        for (Topping concreteTopping : allToppings) {
            Class<?> concreteToppingClass = concreteTopping.getClass();

            assertFalse(Modifier.isAbstract(concreteToppingClass.getModifiers()));
        }
    }

    @Test
    public void testAllToppingsImplementsTopping() {
        for (Topping concreteTopping : allToppings) {
            Class<?> concreteToppingClass = concreteTopping.getClass();

            assertTrue(Topping.class.isAssignableFrom(concreteToppingClass));
        }
    }

    @Test
    public void testAllToppingsHaveDifferentDescription() {
        HashSet<String> allDescriptions = new HashSet<>();

        for (Topping concreteTopping : allToppings) {
            String description = concreteTopping.getDescription();

            assertFalse(allDescriptions.contains(description));
            allDescriptions.add(description);
        }
    }

}
