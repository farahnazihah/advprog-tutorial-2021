package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MondoUdonFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty

    private static MondoUdonFactory mondoUdonFactory;

    @BeforeAll
    public static void setUp() {
        mondoUdonFactory = new MondoUdonFactory();
    }

    @Test
    public void testMondoUdonFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(MondoUdonFactory.class.getModifiers()));
    }

    @Test
    public void testMondoUdonFactoryImplementsIngridientsFactory() {
        assertTrue(IngridientsFactory.class.isAssignableFrom(MondoUdonFactory.class));
    }

    @Test
    public void testMondoUdonFactoryReturnsCorrectIngridients() {
        assertTrue(mondoUdonFactory.getNoodle() instanceof Udon);
        assertTrue(mondoUdonFactory.getMeat() instanceof Chicken);
        assertTrue(mondoUdonFactory.getTopping() instanceof Cheese);
        assertTrue(mondoUdonFactory.getFlavor() instanceof Salty);
    }

}
