package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class OrderServiceImplTest {

    private static OrderServiceImpl orderService;

    @BeforeAll
    static void setUp() throws Exception {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceOrderDrink() throws Exception {
        String drink = "minuman";
        orderService.orderADrink("minuman");

        String result = orderService.getDrink().getDrink();
        assertEquals(drink, result);
    }

    @Test
    public void testOrderServiceOrderFood() throws Exception {
        String food = "makanan";
        orderService.orderAFood("makanan");

        String result = orderService.getFood().getFood();
        assertEquals(food, result);
    }

    @Test
    public void testOrderDrinkInstance() throws Exception {
        assertEquals(OrderDrink.getInstance(), orderService.getDrink());
    }

    @Test
    public void testOrderFoodInstance() throws Exception {
        assertEquals(OrderFood.getInstance(), orderService.getFood());
    }
}
