package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami

    private static SnevnezhaShiratakiFactory snevnezhaShiratakiFactory;

    @BeforeAll
    public static void setUp() {
        snevnezhaShiratakiFactory = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testSnevnezhaShiratakiFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(SnevnezhaShiratakiFactory.class.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryImplementsIngridientsFactory() {
        assertTrue(IngridientsFactory.class.isAssignableFrom(SnevnezhaShiratakiFactory.class));
    }

    @Test
    public void testSnevnezhaShiratakiFactoryReturnsCorrectIngridients() {
        assertTrue(snevnezhaShiratakiFactory.getNoodle() instanceof Shirataki);
        assertTrue(snevnezhaShiratakiFactory.getMeat() instanceof Fish);
        assertTrue(snevnezhaShiratakiFactory.getTopping() instanceof Flower);
        assertTrue(snevnezhaShiratakiFactory.getFlavor() instanceof Umami);
    }

}
