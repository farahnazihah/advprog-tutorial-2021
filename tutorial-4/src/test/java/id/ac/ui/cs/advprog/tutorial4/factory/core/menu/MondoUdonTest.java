package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.MondoUdonFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MondoUdonTest {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty

    private static String udonName;
    private static MondoUdon mondoUdon;

    @BeforeAll
    public static void setUp() {
        udonName = "udonnn";
        mondoUdon = new MondoUdon(udonName);
    }

    @Test
    public void testCorrectIngredients() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }

    @Test
    public void testCorrectName() {
        assertEquals(udonName, mondoUdon.getName());
    }
}
