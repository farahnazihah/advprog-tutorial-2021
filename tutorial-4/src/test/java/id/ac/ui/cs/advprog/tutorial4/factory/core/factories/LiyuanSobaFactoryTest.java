package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaFactoryTest {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet

    private static LiyuanSobaFactory liyuanSobaFactory;

    @BeforeAll
    public static void setUp() {
        liyuanSobaFactory = new LiyuanSobaFactory();
    }

    @Test
    public void testLiyuanSobaFactoryIsConcreteClass() {
        assertFalse(Modifier.isAbstract(LiyuanSobaFactory.class.getModifiers()));
    }

    @Test
    public void testLiyuanSobaFactoryImplementsIngridientsFactory() {
        assertTrue(IngridientsFactory.class.isAssignableFrom(LiyuanSobaFactory.class));
    }

    @Test
    public void testLiyuanSobaFactoryReturnsCorrectIngridients() {
        assertTrue(liyuanSobaFactory.getNoodle() instanceof Soba);
        assertTrue(liyuanSobaFactory.getMeat() instanceof Beef);
        assertTrue(liyuanSobaFactory.getTopping() instanceof Mushroom);
        assertTrue(liyuanSobaFactory.getFlavor() instanceof Sweet);
    }

}
