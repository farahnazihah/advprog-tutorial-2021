package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;

public class InuzumaRamenFactory implements IngridientsFactory {
    // Ingridients:
    // Noodle: Ramen
    // Meat: Pork
    // Topping: Boiled Egg
    // Flavor: Spicy
    public Noodle getNoodle() {
        return new Ramen();
    }

    public Meat getMeat() {
        return new Pork();
    }

    public Topping getTopping() {
        return new BoiledEgg();
    }

    public Flavor getFlavor() {
        return new Spicy();
    }
}
