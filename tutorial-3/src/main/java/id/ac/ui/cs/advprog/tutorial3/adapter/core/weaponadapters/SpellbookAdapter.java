package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private Boolean prevIsCharged;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.prevIsCharged = false;

    }

    @Override
    public String normalAttack() {
        this.prevIsCharged = false;

        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (prevIsCharged)
            return "Can't cast this spell twice in a row";
        else {
            this.prevIsCharged = true;
            return this.spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
